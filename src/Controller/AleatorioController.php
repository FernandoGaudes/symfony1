<?php
//src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AleatorioController extends AbstractController
{


    public function number()
    {
        $number=random_int(1,100);

        return $this->render('aleatorio/number.html.twig', array('number'=>$number,));
    }
}