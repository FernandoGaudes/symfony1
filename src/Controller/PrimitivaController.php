<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class PrimitivaController extends Controller
{
    /**
     * @Route("/primitiva")
     */

    public function combinacion()
    {
        $combinacion=[];
        for($i=1;$i<=8;$i++){
            $number=random_int(1,49);
            if (in_array($number, $combinacion) ){
                $i--;
            }else{
               array_push($combinacion,$number); 
            }  
        }

        return new Response('
            <html>
                <body>
                    <h1>La combinación ganadora de la primitiva.</h1>
                    <p>'.$combinacion[0].' , '.$combinacion[1].' , '.$combinacion[2].' , '.$combinacion[3].' , '.$combinacion[4].' , '.$combinacion[5].'<p>
                    <p> Complementario: '.$combinacion[6].' <p>
                    <p> Reintegro: '.$combinacion[7].' <p>
                </body>
            </html>
        ');
    }
}