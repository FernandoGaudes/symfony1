<?php
//src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;



class DefaultController extends AbstractController
{
    /**
     * @Route("/")
     */

    public function index()
    {
        return new Response('
            <html>
                <body>
                    <h1>Hello a tod@s</h1>
                </body>
            </html>
        ');
    }
    /**
     * @Route("/ejemplo")
     */
    public function ejemplo()
    {
        return new Response('
            <html>
                <body>
                    <h1>Esto es un ejemplo.</h1>
                </body>
            </html>
        ');
    }   

}